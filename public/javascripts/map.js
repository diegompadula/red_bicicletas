var mymap = L.map('main_map').setView([-31.638761, -60.687951], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(mymap);

L.marker([-31.636944, -60.689619]).addTo(mymap);
L.marker([-31.638445, -60.687367]).addTo(mymap);
L.marker([-31.642683, -60.691637]).addTo(mymap);

$.ajax({
  datatype: 'json',
  url: 'api/bicicletas',
  success: function (result) {
    console.log(result);
    result.bicis.forEach(function (bici) {
      L.marker(bici.ubicacion, { title: bici.id + '-' + bici.color }).addTo(
        mymap
      );
    });
  },
});
