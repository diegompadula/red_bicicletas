**Proyecto Red_Bicicletas de Coursera**

Este archivo README intenta explicar el contenido del proyecto de desarrollo WEB con NodeJS y Express del curso en Coursera de la Universidad Austral.

---

## Contenidos de aprendizaje

Contenidos desarrollados en el curso

1. Conceptos básicos del desarrollo web del lado servidor.
2. Persistencia del modelo utilizando Mongoose y MongoDB.
3. Autenticación.
4. Oauth y Puesta en producción en Heroku.

https://bitbucket.org/diegompadula/red_bicicletas/src/master/
