var express = require('express');
var router = express.Router();
var bicicletaControllerAPI = require('../../controllers/api/bicicletaControllerAPI');

/* GET bicis listing. */
router.get('/', bicicletaControllerAPI.bicicleta_list);
router.post('/create', bicicletaControllerAPI.bicicleta_create);
router.post('/update', bicicletaControllerAPI.bicicleta_update);
router.post('/delete', bicicletaControllerAPI.bicicleta_delete);

module.exports = router;
