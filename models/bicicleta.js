var Bicicleta = function (id, color, modelo, ubicacion) {
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
};

Bicicleta.prototype.toString = function () {
  return 'id: ' + this.id + ' | color: ' + this.color;
};

Bicicleta.allBicis = [];

Bicicleta.add = function (aBici) {
  Bicicleta.allBicis.push(aBici);
};

Bicicleta.findById = function (aBiciId) {
  //   console.log('Bicicleta.findById ', aBiciId);
  var bici = Bicicleta.allBicis.find((x) => x.id == aBiciId);
  //   console.log('Bicicleta.findById ', bici);
  if (bici) return bici;
  else throw new Error(`No existe una bicicleta con ID: ${aBiciId}`);
};

Bicicleta.removeById = function (aBiciId) {
  for (var i = 0; i < Bicicleta.allBicis.length; i++) {
    if (Bicicleta.allBicis[i].id == aBiciId) {
      Bicicleta.allBicis.splice(i, 1);
      break;
    }
  }
};

var unaBici = new Bicicleta(1, 'roja', 'urbana', [-31.635535, -60.688696]);
var otraBici = new Bicicleta(2, 'azul', 'carrera', [-31.633423, -60.692418]);

Bicicleta.add(unaBici);
Bicicleta.add(otraBici);

module.exports = Bicicleta;
